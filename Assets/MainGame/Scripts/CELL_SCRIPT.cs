﻿using UnityEngine;
using System.Collections;

public class CELL_SCRIPT : MonoBehaviour {

    
    GameObject[][] massCell;
    GameManagerScript _GMS;

    void Awake()
    {
       GameObject gameManager = GameObject.Find("GameManager");
        if (gameManager != null)
        {
            
            _GMS = gameManager.GetComponent<GameManagerScript>();
        }

        massCell = new GameObject [6][];
        findCellObjects();

       
    }



    //In this method, I form a cell arrayas an array of game objects
    void findCellObjects()
    {
        
        massCell[0]= new GameObject[7];
        massCell[1] = new GameObject[7];
        massCell[2] = new GameObject[7];
        massCell[3] = new GameObject[7];
        massCell[4] = new GameObject[7];
        massCell[5] = new GameObject[7];
       print("massCell.Length = " + massCell.Length);
      
        for (int i = 0; i < massCell.Length; i++)
        {
            for (int j = 0; j < massCell[i].Length; j++ )
            {
                massCell[i][j] = GameObject.Find("Cell " + (j + i * (massCell[i].Length)));
                print("length = " + massCell[i][j]);
               
            }
        }
      
    }


   //In this method, I sort out all the cells of the field after each move
    public void scanBoard()  

    {
        //for vertical scan
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < massCell[i].Length; j++)
            {

                if (massCell[i][j].tag != "0" &&
                    massCell[i][j].tag == (massCell[i + 1][j].tag) &&
                    massCell[i][j].tag == (massCell[i + 2][j].tag) &&
                    massCell[i][j].tag == (massCell[i + 3][j].tag))
                {
                    _GMS.levelCompleted(massCell[i][j].tag); // WIN !

                }
            }
        }


        // for horizontal scan
        for (int i = 0; i < massCell.Length; i++)
        {
            for (int j = 0; j < 4; j++)
            {

                if (massCell[i][j].tag != "0" &&
                    massCell[i][j].tag == massCell[i][j + 1].tag &&
                    massCell[i][j].tag == massCell[i][j + 2].tag &&
                    massCell[i][j].tag == massCell[i][j + 3].tag)
                {

                    _GMS.levelCompleted(massCell[i][j].tag);  // WIN !

                }
            }
        }


        // for right and down scan
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 4; j++)
            {

                if (massCell[i][j].tag != "0" &&
                    massCell[i][j].tag == massCell[i + 1][j + 1].tag &&
                    massCell[i][j].tag == massCell[i + 2][j + 2].tag &&
                    massCell[i][j].tag == massCell[i + 3][j + 3].tag)
                {
                    _GMS.levelCompleted(massCell[i][j].tag);  // WIN !

                }
            }
        }


        // for right and up scan
        for (int i = 3; i < massCell.Length; i++)
        {
            for (int j = 0; j < 4; j++)
            {

                if (massCell[i][j].tag != "0" &&
                    massCell[i][j].tag == massCell[i - 1][j + 1].tag &&
                    massCell[i][j].tag == massCell[i - 2][j + 2].tag &&
                    massCell[i][j].tag == massCell[i - 3][j + 3].tag)
                {
                    _GMS.levelCompleted(massCell[i][j].tag);  // WIN !

                }
            }
        }

    }

}
