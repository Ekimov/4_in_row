﻿using UnityEngine;
using System.Collections;

public class InCellMove : MonoBehaviour {

    
    [HideInInspector]
    public bool isCentr = false;
    GameObject player;
    CELL_SCRIPT _CS;
    GameObject gameManager;
    GameManagerScript _GMS;
    
   

    void Awake()
    {
        
        gameManager = GameObject.Find("GameManager");
        if (gameManager != null)
        {
           
            _CS = gameManager.GetComponent<CELL_SCRIPT>();
        }
      
    }

	void Start () {
        //print("Transform.position x = " + transform.position.x);
	}
	
	
	void FixedUpdate () {

        if (isCentr)
        {
            float dist = (player.transform.position - transform.position).sqrMagnitude;
           
            if (dist <= 0.1f)
            {
                if (player.GetComponent<Rigidbody2D>().velocity.y == 0)
                {
                    //print("IF");
                    if (player.gameObject.tag == "Red")
                    {
                        
                        this.gameObject.tag = "redCell";
                        _CS.scanBoard();
                        this.enabled = false;
                    }
                    if (player.gameObject.tag == "Blue")
                    {
                        
                        this.gameObject.tag = "blueCell";
                        _CS.scanBoard();
                        this.enabled = false;
                    }
                }
            }
            

            if (dist <= 0.01f && player.gameObject.tag == "Cursor")
            {
              
                player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                player.transform.position = transform.position;
                isCentr = false;
            }
           
        } 
	}


    void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.tag == "Cursor" || coll.tag == "Red" || coll.tag=="Blue")
        {
            player = coll.gameObject; 
            isCentr = true;
            
        }
    }

   
}
