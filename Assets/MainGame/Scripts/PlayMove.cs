﻿using UnityEngine;
using System.Collections;

public class PlayMove : MonoBehaviour {

    public float speed;
    DownScript _DS; 
	
	void Start () {
        _DS = GetComponentInChildren<DownScript>();
        GetComponent<Rigidbody2D>().velocity = Vector3.down * speed * Time.fixedDeltaTime;
       
	}
	
	
	void Update () {
        if (_DS.isTouchBorder)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
        }
	}
}
