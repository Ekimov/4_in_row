﻿using UnityEngine;
using System.Collections;

public class MoveCursor : MonoBehaviour {

    public Rigidbody2D cursor;

    private Touch initialTouch = new Touch();
    private float distance = 0;
    private bool hasSwiped = false;

    public float speed = 10;
    [HideInInspector]
    public bool dontSwipe = false;

    void Update()
    {
        if ((cursor.velocity.x != 0 || cursor.velocity.y != 0) )
        {
            dontSwipe = true;

        }
        else
        {
            dontSwipe = false;
        }

        buttonControlPC();

        swipeMove();

    }

    //this method to control the cursor on a device
    void swipeMove()
    {

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            {
                switch (touch.phase)
                {
                    case TouchPhase.Began:

                        initialTouch = touch;
                        break;

                    case TouchPhase.Moved:
                        if (!hasSwiped)
                        {
                            float deltaX = initialTouch.position.x - touch.position.x;
                            float deltaY = initialTouch.position.y - touch.position.y;
                            distance = Mathf.Sqrt((deltaX * deltaX) + (deltaY * deltaY));
                            bool swipedSideways = Mathf.Abs(deltaX) > Mathf.Abs(deltaY);

                            if (distance > 4f)
                            {
                                if (!dontSwipe)
                                {
                             
                                    if (swipedSideways && deltaX > 0) //swiped left
                                    {
                                        if (cursor.gameObject.activeSelf && !cursor.GetComponentInChildren<LeftScript>().isTouchBorder)
                                        {
                                            cursor.velocity = Vector3.left * speed * Time.fixedDeltaTime;
                                        }
                                   
                                    }
                                    else if (swipedSideways && deltaX <= 0) //swiped right
                                    {



                                        if (cursor.gameObject.activeSelf && !cursor.GetComponentInChildren<RightScript>().isTouchBorder)
                                        {
                                            cursor.velocity = Vector3.right * speed * Time.fixedDeltaTime;
                                        }
                                       
                                    }
                                   
                                    hasSwiped = true; // only for Moved!
                                }
                            }

                        }


                        break;

                    case TouchPhase.Ended:
                        initialTouch = new Touch();
                        hasSwiped = false;
                        break;

                }
            }


        }


    }

    //this method to control the cursor on a device
    void buttonControlPC()
    {
        if (!dontSwipe)
        {
            if (Input.GetKey(KeyCode.A)) // left
            {
                if (cursor.gameObject.activeSelf && !cursor.GetComponentInChildren<LeftScript>().isTouchBorder)
                {
                    cursor.velocity = Vector3.left * speed * Time.fixedDeltaTime;
                }
                
            }
            
            if (Input.GetKey(KeyCode.D)) //right
            {


                if (cursor.gameObject.activeSelf && !cursor.GetComponentInChildren<RightScript>().isTouchBorder)
                {
                    cursor.velocity = Vector3.right * speed * Time.fixedDeltaTime;
                    
                }
                
            }
          
        }
    }
}
