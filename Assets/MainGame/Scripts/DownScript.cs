﻿using UnityEngine;
using System.Collections;

public class DownScript : MonoBehaviour
{

    [HideInInspector]
    public bool isTouchBorder = false;
    GameObject border;

    void Awake()
    {
       // print("Awake isTouch " + isTouchBorder);

    }
    void FixedUpdate()
    {
        if (border && !border.activeSelf)
        {
            isTouchBorder = false;
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.tag == "Border" || coll.tag == "redCell" || coll.tag == "blueCell")
        {
            border = coll.gameObject;
            isTouchBorder = true;

        }

    }
  
}
