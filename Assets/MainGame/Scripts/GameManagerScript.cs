﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour
{

    public Canvas gameOverCanvas;
    public Canvas gamePlay;
    public Canvas CanvasLevelCompleted;
    public Text win;
    public bool nextPlayer;
    public Rigidbody2D cursor;
    public GameObject player1, player2;

   
    public void ReplayLevel(string nameLevel)
    {

        Application.LoadLevel(nameLevel);
    }


    public void levelCompleted(string tag)
    {
        gamePlay.gameObject.SetActive(false);
        switch (tag)
        {
            case "blueCell":
                win.text = "BLUE WIN !";
                win.color = Color.blue;

                break;
            case "redCell":
                win.text = "RED WIN !";
                win.color = Color.red;
                break;

        }
        CanvasLevelCompleted.gameObject.SetActive(true);
        GetComponent<MoveCursor>().enabled = false;
    }

    //for 4 in row
    public void instatiatePlayer()
    {
        if (nextPlayer)
        {
            Instantiate(player1, cursor.transform.position, Quaternion.identity);

        }
        else
        {
            Instantiate(player2, cursor.transform.position, Quaternion.identity);
        }
        nextPlayer =!nextPlayer;

    }
}

