﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class OnClickLevel : MonoBehaviour, IPointerClickHandler{

    public string NameLevel;

    public void OnPointerClick(PointerEventData data)
    {
        print("CLICK");
        LevelLoad(NameLevel);

    }

    void LevelLoad(string name)
    {
        Application.LoadLevel(name);
    }
}
